<?php
/**
 * @Author: 陈静
 * @Date: 2018/08/20 20:18:45
 * @Description:
 */

namespace App\Component\Auth\Account;

use App\Models\Dao\AdminDao;
use App\Models\Data\AdminData;
use Swoft\Auth\Bean\AuthResult;
use Swoft\Auth\Mapping\AccountTypeInterface;
use Swoft\Bean\Annotation\Bean;
use Swoft\Bean\Annotation\Inject;

/**
 * Class AdminAccount
 * @package App\Component\Auth\Account
 * @Bean()
 */
class AdminAccount implements AccountTypeInterface
{
    /**
     * @Inject()
     * @var AdminDao
     */
    private $AdminDao;
    /**
     * @param array $data
     * @return AuthResult|null|\Swoft\Auth\Bean\AuthResult
     */
    public function login(array $data):AuthResult
    {
        $userInfo = $this->AdminDao->getUserInfoByField('username',$data['username'],['id','username','password']);

        $res = new AuthResult();
        if ( $userInfo && (new AdminData())->verifyPassword( $data['password'], $userInfo->getPassword() ) ) {
            $res->setExtendedData(['role'=>'services']);
            $res->setIdentity( $userInfo->getId() );
        }

        return $res;
    }

    public function authenticate(string $identity): bool
    {
        return true;
    }

}