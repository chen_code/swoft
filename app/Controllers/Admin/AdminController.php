<?php
/**
 * @Author: 陈静
 * @Date: 2018/08/15 20:09:06
 * @Description:
 */

namespace App\Controllers\Admin;
use App\Domain\User\Service\AuthManagerService;
use App\Models\Dao\AdminDao;
use Swoft\App;
use Swoft\Auth\Bean\AuthSession;
use Swoft\Auth\Helper\ErrorCode;
use Swoft\Auth\Mapping\AuthManagerInterface;
use Swoft\Auth\Middleware\AclMiddleware;
use Swoft\Bean\Annotation\Inject;
use Swoft\Http\Message\Bean\Annotation\Middleware;
use Swoft\Http\Message\Server\Request;
use Swoft\Http\Server\Bean\Annotation\Controller;
use Swoft\Http\Server\Bean\Annotation\RequestMapping;
use Swoft\Http\Server\Bean\Annotation\RequestMethod;
use App\Middlewares\CorsMiddleware;
use App\Middlewares\JsonResponseMiddleware;

/**
 * Class AdminController
 * @Controller(prefix="/Admin")
 * @package App\Controllers\Admin
 */
class AdminController
{
    /**
     * @Middleware(AclMiddleware::class)
     * @RequestMapping(route="user")
     */
    public function test()
    {
        dump('test');
    }

    /**
     * @Middleware(CorsMiddleware::class)
     * @RequestMapping(route="user", method={RequestMethod::POST})
     * @param Request $request
     * @return array
     */
    public function login(Request $request)
    {
        $username = $request->post('username','');
        $password = $request->post('password','');

        if(!$username || !$password){
            return [
                "code"=>ErrorCode::POST_DATA_NOT_PROVIDED,
                "message"=>"not recieve post data"
            ];
        }
        /** @var AuthManagerService $manager */
        $manager = App::getBean(AuthManagerInterface::class);
        /** @var AuthSession $session */
        $session = $manager->adminBasicLogin($username,$password);
        $data = [
            'code' => 0,
            'msg' => 'success',
            'data' => [
                'access_token'=>$session->getToken(),
                'expire'=>$session->getExpirationTime()
            ]
        ];
        return $data;
    }

    /**
     * @Inject()
     * @var AdminDao
     */
    private $adminDao;
    /**
     * @Middleware(JsonResponseMiddleware::class)
     * @RequestMapping(route="users", method=RequestMethod::POST)
     * @return array
     */
    public function login1(Request $request)
    {
        $userId = $request->post('user_id',0);
        $userInfo = $this->adminDao->getUserInfoByField('id',$userId,['id','username','head_pic']);

        return [
            0, 'success', $userInfo->getAttrs(),
        ];

    }

}