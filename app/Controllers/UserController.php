<?php
/**
 * This file is part of Swoft.
 *
 * @link https://swoft.org
 * @document https://doc.swoft.org
 * @contact group@swoft.org
 * @license https://github.com/swoft-cloud/swoft/blob/master/LICENSE
 */

namespace App\Controllers;

use App\Models\Dao\UserDao;
use App\WebSocket\Common\MaintainRelationship;
use Swoft\App;
use Swoft\Bean\Annotation\Inject;
use Swoft\Http\Message\Bean\Annotation\Middleware;
use Swoft\Http\Message\Server\Request;
use Swoft\Http\Server\Bean\Annotation\Controller;
use Swoft\Http\Server\Bean\Annotation\RequestMapping;
use Swoft\Http\Server\Bean\Annotation\RequestMethod;
use App\Middlewares\JsonResponseMiddleware;

/**
 * Class UserController
 * @Controller(prefix="/Users")
 * @package App\Controllers
 */
class UserController{

    /**
     * @Inject()
     * @var MaintainRelationship
     */
    private $maintainRelationship;

    /**
     * @Inject()
     * @var UserDao
     */
    private $userDao;
    /**
     * @Middleware(JsonResponseMiddleware::class)
     * @RequestMapping(route="users", method=RequestMethod::POST)
     * @return array
     */
    public function login(Request $request)
    {
        $userId = $request->post('user_id',0);
        $userInfo = $this->userDao->getUserInfoByField('id',$userId,['id','username','head_pic']);

        return [
            0, 'success', $userInfo->getAttrs(),
        ];

    }

    //获取在线人数
    /**
     * @RequestMapping(route="getOnlineMembers",method={RequestMethod::POST})
     * @Middleware(JsonResponseMiddleware::class)
     */
    public function getOnlineMembers(Request $request)
    {
        $identity = $request->post('identity','');
        $onlineMembers = $this->maintainRelationship->getOnlineMembers($identity);
        return [0,'success',$onlineMembers];
    }



}
