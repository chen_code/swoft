<?php
/**
 * @Author: 陈静
 * @Date: 2018/08/20 20:29:00
 * @Description:
 */

namespace App\Domain\User\Service;


use App\Component\Auth\Account\AdminAccount;
use App\Exception\ApiException;
use Swoft\Auth\AuthManager;
use Swoft\Auth\Bean\AuthResult;
use Swoft\Auth\Helper\ErrorCode;
use Swoft\Auth\Mapping\AuthManagerInterface;
use Swoft\Redis\Redis;
use Swoft\Auth\Bean\AuthSession;
use Psr\SimpleCache\InvalidArgumentException;

class AuthManagerService extends AuthManager implements AuthManagerInterface
{
    /**
     * @var string
     */
    protected $cacheClass = Redis::class;
    /**
     * @var bool 开启缓存
     */
    protected $cacheEnable = true;

    /**
     * @param string $username
     * @param string $password
     * @return \Swoft\Auth\Bean\AuthSession
     */
    public function adminBasicLogin(string $username,string $password){
        return $this->login(AdminAccount::class,[
            'username' => $username,
            'password' => $password
        ]);
    }

    /**
     * @param $accountTypeName
     * @param array $data
     * @return AuthSession
     */
    public function login(string $accountTypeName, array $data):AuthSession
    {
        if (!$account = $this->getAccountType($accountTypeName)) {
            throw new ApiException(ErrorCode::AUTH_INVALID_ACCOUNT_TYPE);
        }
        $result = $account->login($data);
        if (!$result instanceof AuthResult || $result->getIdentity() == '') {
            throw new ApiException(ErrorCode::AUTH_LOGIN_FAILED);
        }
        $session = $this->generateSession($accountTypeName, $result->getIdentity(), $result->getExtendedData());
        $this->setSession($session);
        if ($this->cacheEnable === true) {
            try {
                $this->getCacheClient()->set(
                    $this->getCacheKey($result->getIdentity()),
                    $session->getToken(),
                    $session->getExpirationTime()
                );
            } catch (InvalidArgumentException $e) {
                $err = sprintf('%s Invalid Argument : %s', $session->getIdentity(), $e->getMessage());
                throw new ApiException(ErrorCode::POST_DATA_NOT_PROVIDED, $err);
            }
        }
        return $session;
    }


}