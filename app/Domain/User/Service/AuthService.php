<?php
/**
 * @Author: 陈静
 * @Date: 2018/08/21 15:01:52
 * @Description:
 */

namespace App\Domain\User\Service;


use Psr\Http\Message\ServerRequestInterface;
use Swoft\Auth\AuthUserService;
use Swoft\Auth\Mapping\AuthServiceInterface;
use Swoft\Bean\Annotation\Bean;

/**
 * Class AuthService
 * @package App\Domain\User\Service
 * @Bean()
 */
class AuthService extends AuthUserService implements AuthServiceInterface
{
    public function auth(string $requestHandler, ServerRequestInterface $request): bool
    {
        $id = $this->getUserIdentity();
        $role = $this->getUserExtendData()['role'] ?? '' ;
        echo sprintf(" 你访问了: %s",$requestHandler);
        if($id){
            return true;
        }
        return false;
    }

}