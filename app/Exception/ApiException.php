<?php
/**
 * @Author: 陈静
 * @Date: 2018/08/22 17:10:23
 * @Description:
 */

namespace App\Exception;


use Swoft\Exception\RuntimeException;
use \Throwable;

/**
 * Class ApiException
 * @package App\Exception
 */
class ApiException extends RuntimeException
{
    public function __construct(int $code = 0, string $message = '', Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}