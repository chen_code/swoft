<?php
/**
 * @Author: 陈静
 * @Date: 2018/08/22 16:58:16
 * @Description:
 */

namespace App\Exception\Handlers;

use Swoft\App;
use Swoft\Bean\Annotation\ExceptionHandler;
use Swoft\Bean\Annotation\Handler;
use Swoft\Http\Message\Server\Response;
use App\Exception\ApiException;

/**
 * Class ApiExceptionHandler
 * @ExceptionHandler()
 * @package App\Exception
 */
class ApiExceptionHandler
{
    /**
     * @Handler(ApiException::class)
     * @param Response   $response
     * @param \Throwable $throwable
     * @return Response
     */
    public function handlerException(Response $response, \Throwable $throwable)
    {
        $file      = $throwable->getFile();
        $line      = $throwable->getLine();
        $code      = $throwable->getCode();
        $exception = $throwable->getMessage();
        $data = ['msg' => $exception, 'file' => $file, 'line' => $line, 'code' => 1001];
        App::error(json_encode($data));
        return $response->json($data);
    }
}