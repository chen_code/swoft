<?php
/**
 * @Author: 陈静
 * @Date: 2018/08/15 20:30:46
 * @Description: 设置跨域中间件
 */
namespace App\Middlewares;

use Swoft\Http\Message\Middleware\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Swoft\Bean\Annotation\Bean;

/**
 * @Bean()
 */
class JsonResponseMiddleware implements MiddlewareInterface
{
    /**
     * Process an incoming server request and return a response, optionally delegating
     * response creation to a handler.
     *
     * @param \Psr\Http\Message\ServerRequestInterface $request
     * @param \Psr\Http\Server\RequestHandlerInterface $handler
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \InvalidArgumentException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $response = $handler->handle($request);
        $responseDatas = $response->getAttribute('responseAttribute');
        $response = $response->withAttribute('responseAttribute',[
            'code' => $responseDatas[0] ,
            'msg' => $responseDatas[1] ,
            'data' => $responseDatas[2] ,
        ]);
        return $response;
    }

}
