<?php
/**
 * @Author: 陈静
 * @Date: 2018/08/21 19:38:15
 * @Description:
 */

namespace App\Models\Dao;
use App\Models\Entity\Admin;
use Swoft\Bean\Annotation\Bean;

/**
 * Class AdminDao
 * @package App\Models\Dao
 * @Bean()
 * @uses AdminDao
 */
class AdminDao
{
    public function getUserInfoByField(string $fieldName , $fieldValue ,array $fields = [])
    {
        return Admin::findOne([$fieldName => $fieldValue],['fields' => $fields])->getResult();
    }

}