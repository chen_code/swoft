<?php
/**
 * @Author: 陈静
 * @Date: 2018/08/24 12:44:48
 * @Description:
 */

namespace App\Models\Dao;
use App\Models\Entity\User;
use Swoft\Bean\Annotation\Bean;

/**
 * Class UserDao
 * @package App\Models\Dao
 * @Bean()
 * @uses UserDao
 */
class UserDao
{
    public function getUserInfoByField(string $fieldName , $fieldValue ,array $fields = [])
    {
        return User::findOne([$fieldName => $fieldValue],['fields' => $fields])->getResult();
    }
}