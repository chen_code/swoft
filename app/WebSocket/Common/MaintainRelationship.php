<?php
/**
 * @Author: 陈静
 * @Date: 2018/08/26 10:45:11
 * @Description:
 */

namespace App\WebSocket\Common;

use App\Models\Dao\UserDao;
use App\Models\Entity\Admin;
use App\Models\Entity\User;
use Swoft\Bean\Annotation\Bean;
use Swoft\Redis\Redis;

/**
 * Class MaintainRelationship
 * @package App\WebSocket\Common
 * @Bean()
 */
class MaintainRelationship
{
    private $redis;
    private $userDao;

    public function __construct()
    {
        $this->redis = new Redis();
        $this->userDao = new UserDao();
    }

    //有序集合维护user和fd之间的关系
    public function onConnect( $keyName , $userId , $fd )
    {
        //添加新的关系
        $this->redis->zAdd($keyName , $userId,$fd);
    }

    public function getOnlineMembers($identity)
    {
        $onlineMembersIdArr = $this->redis->ZRANGE($identity,0,-1,true);

        //区分客服和顾客
        if ($identity == 'customer') {
            $memberModel = new User();
        }else{
            $memberModel = new Admin();
        }
        $userInfoArr = [];
        if ($onlineMembersIdArr) {
            $userInfoArr = $memberModel::findByIds(array_unique($onlineMembersIdArr),[ 'fields' => ['id','username','head_pic']])
                ->getResult()->toArray();
            foreach ($userInfoArr as &$v) {
                $v['identity'] = 'services';
                $v['avatar'] = $v['headPic'];
                $v['status'] = 'online';
                unset($v['headPic']);
            }
        }

        return $userInfoArr;
    }
}