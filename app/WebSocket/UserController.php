<?php
/**
 * @Author: 陈静
 * @Date: 2018/08/26 09:25:09
 * @Description:
 */

namespace App\WebSocket;


use App\WebSocket\Common\MaintainRelationship;
use Swoft\App;
use Swoole\WebSocket\Frame;
use Swoole\WebSocket\Server;

/**
 * Class UserController
 * @package App\WebSocket
 */
class UserController
{
    private $maintainRelationship;
    public function __construct()
    {
        $this->maintainRelationship = App::getBean(MaintainRelationship::class);
    }
    //关联关系
    public function onConnect(Server $server,Frame $frame,$params = [])
    {
        $this->maintainRelationship->onConnect($params['identity'], $params['user_id'] , $frame->fd);
    }

}