<?php

/*
 * This file is part of Swoft.
 * (c) Swoft <group@swoft.org>
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [
    'serverDispatcher' => [
        'middlewares' => [
            \App\Middlewares\CorsMiddleware::class,  //跨域请求
            \Swoft\Auth\Middleware\AuthMiddleware::class,  //后台验证
//            \Swoft\Devtool\Middleware\DevToolMiddleware::class,  //devtool
        ]
    ],
    \Swoft\Auth\Mapping\AuthManagerInterface::class=>[
        'class'=>App\Domain\User\Service\AuthManagerService::class  //后台验证
    ],
    \Swoft\Auth\Mapping\AuthServiceInterface::class=>[
        'class'=>App\Domain\User\Service\AuthService::class  //后台验证
    ],
    'httpRouter'       => [
        'ignoreLastSlash'  => false,
        'tmpCacheNumber' => 1000,
        'matchAll'       => '',
    ],
    'requestParser'    => [
        'parsers' => [

        ],
    ],
    'view'             => [
        'viewsPath' => '@resources/views/',
    ],
    'cache'            => [
        'driver' => 'redis',
    ],
];
