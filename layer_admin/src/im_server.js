/**

 @Name：layuiAdmin im服务
 @Author：攸乐
 @Site：http://www.layui.com/admin/
 @License：LPPL
 */

var wsServer = 'ws://120.78.69.174:9501';
var websocket = new WebSocket(wsServer);
websocket.onopen = function (evt) {
    addLine("Connected to WebSocket server.");
};

websocket.onclose = function (evt) {
    addLine("Disconnected");
};

websocket.onmessage = function (evt) {
    addLine('Retrieved data from server: ' + evt.data);
};

websocket.onerror = function (evt, e) {
    addLine('Error occured: ' + evt.data);
};
